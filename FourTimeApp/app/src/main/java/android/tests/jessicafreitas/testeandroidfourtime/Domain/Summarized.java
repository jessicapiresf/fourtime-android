package android.tests.jessicafreitas.testeandroidfourtime.Domain;

/**
 * Created by Jessica on 08/08/16.
 */
public class Summarized {
    String month;
    String value;

    public Summarized(String month, String value){
        setMonth(month);
        setValue(value);
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
