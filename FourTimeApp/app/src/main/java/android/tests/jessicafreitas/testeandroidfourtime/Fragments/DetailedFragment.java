package android.tests.jessicafreitas.testeandroidfourtime.Fragments;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.tests.jessicafreitas.testeandroidfourtime.Adapter.DetailedAdapter;
import android.tests.jessicafreitas.testeandroidfourtime.Api.MonthApi;
import android.tests.jessicafreitas.testeandroidfourtime.ConnectionErrorActivity_;
import android.tests.jessicafreitas.testeandroidfourtime.Domain.Month;
import android.tests.jessicafreitas.testeandroidfourtime.Domain.Purchase;
import android.tests.jessicafreitas.testeandroidfourtime.MainActivity;
import android.tests.jessicafreitas.testeandroidfourtime.R;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EFragment;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Jessica on 08/08/16.
 */

@EFragment(R.layout.fragment_detailed)
public class DetailedFragment extends Fragment {

    View myView;
    int atualMonth = 0;
    Retrofit retrofit;
    MonthApi monthAPI;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ((MainActivity) getActivity()).setTitle(getResources().getString(R.string.menu_extrato_detalhado));
        View view = inflater.inflate(R.layout.fragment_detailed, container, false);

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();

        retrofit = new Retrofit.Builder()
                .baseUrl(MonthApi.ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        monthAPI = retrofit.create(MonthApi.class);

        myView = view;
        loadMonths();
        return view;
    }


    private void loadPurchase(List<Purchase.purchaseClass> purchaseList) {

        Purchase.purchaseClass[] purchaseArray = purchaseList.toArray(new Purchase.purchaseClass[purchaseList.size()]);
        DetailedAdapter adapter = new DetailedAdapter(getContext(), R.layout.row_detailed, purchaseArray, false);

        ListView lv = (ListView) myView.findViewById(R.id.list);
        lv.setAdapter(adapter);
    }

    private void loadMonths() {

        final HorizontalScrollView hsl = (HorizontalScrollView) myView.findViewById(R.id.horizontalScroll);

        final LinearLayout ll = ((LinearLayout) myView.findViewById(R.id.list_month));
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        final int width = size.x;

        String[] months = {"Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul"};
        atualMonth = months.length - 1;

        for (int i = 0; i < months.length; i++) {
            final View b = View.inflate(getContext(), R.layout.row_month, null);
            final TextView month = (TextView) b.findViewById(R.id.textMonth);
            month.setText(months[i]);

            b.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    callServiceGetMonthDetailById(((LinearLayout) v.getParent()).indexOfChild(v) + 1);

                    for (int i = 0; i < ll.getChildCount(); i++) {
                        View viewT = ll.getChildAt(i);
                        final TextView textT = (TextView) viewT.findViewById(R.id.textMonth);
                        textT.setAlpha((float) 0.6);
                        textT.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                    }

                    month.setAlpha(1);
                    month.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                    int scrollX = (b.getLeft() - (width / 2)) + (b.getWidth() / 2);
                    hsl.smoothScrollTo(scrollX, 0);
                }
            });
            ll.addView(b);

            if (atualMonth == i) {
                callServiceGetMonthDetailById(atualMonth + 1);
                month.setAlpha(1);
                month.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            }
        }

        myView.post(new Runnable() {
                        @Override
                        public void run() {
                            ll.refreshDrawableState();
                            int scrollX = (ll.getChildAt(atualMonth).getLeft() - (width / 2)) + (ll.getChildAt(atualMonth).getWidth() / 2);
                            hsl.smoothScrollTo(scrollX, 0);
                        }
                    }
        );

    }

    @Background
    public void callServiceGetAllMonths() {

        Call<Month> call2 = monthAPI.getMonths();

        call2.enqueue(new Callback<Month>() {

            @Override
            public void onResponse(Call<Month> call, Response<Month> response) {
                int code = response.code();
                if (code == 200) {
                    Month user = response.body();
                }
            }

            @Override
            public void onFailure(Call<Month> call, Throwable t) {
            }
        });

    }

    LinearLayout linlaHeaderProgress;

    @Background
    public void callServiceGetMonthDetailById(int month) {

        linlaHeaderProgress = (LinearLayout) myView.findViewById(R.id.linlaHeaderProgress);

        linlaHeaderProgress.setVisibility(View.VISIBLE);

        Call<Purchase> call2 = monthAPI.getPurchase(month);

        call2.enqueue(new Callback<Purchase>() {

            @Override
            public void onResponse(Call<Purchase> call, Response<Purchase> response) {
                int code = response.code();
                if (code == 200) {
                    Purchase user = response.body();
                    loadPurchase(user.purchaseList);
                    linlaHeaderProgress.setVisibility(View.GONE);

                }
            }

            @Override
            public void onFailure(Call<Purchase> call, Throwable t) {

                Intent mainIntent = new Intent(getActivity(), ConnectionErrorActivity_.class);
                getActivity().startActivity(mainIntent);
                getActivity().finish();

            }
        });
    }


}