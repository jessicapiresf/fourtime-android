package android.tests.jessicafreitas.testeandroidfourtime.Domain;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Jessica on 09/08/16.
 */
public class Month {

    @SerializedName(value="list")
    public List<monthClass> MonthList;

    public void setPurchase(List<monthClass> month) {
        this.MonthList = month;
    }

    public class monthClass {

        Map<Integer, String> myMap = new HashMap<Integer, String>();

        public Map<Integer, String> getMyMap() {
            return myMap;
        }

        public void setMyMap(Map<Integer, String> myMap) {
            this.myMap = myMap;
        }
    }



}
