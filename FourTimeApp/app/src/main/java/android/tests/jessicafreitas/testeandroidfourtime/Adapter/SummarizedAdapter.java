package android.tests.jessicafreitas.testeandroidfourtime.Adapter;

import android.content.Context;
import android.tests.jessicafreitas.testeandroidfourtime.Domain.Summarized;
import android.tests.jessicafreitas.testeandroidfourtime.R;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * Created by Jessica on 08/08/16.
 */
public class SummarizedAdapter extends ArrayAdapter<Summarized> {

    Context mContext;
    int layoutResourceId;
    Summarized data[] = null;

    public SummarizedAdapter(Context context, int resource, Summarized[] data, boolean profile) {
        super(context, resource, data);

        this.layoutResourceId = resource;
        this.mContext = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView==null){
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(layoutResourceId, parent, false);
        }

        Summarized sales = data[position];

        TextView month = (TextView) convertView.findViewById(R.id.month);
        TextView value = (TextView) convertView.findViewById(R.id.value);

        month.setText(sales.getMonth());
        value.setText(sales.getValue());

        return convertView;

    }

    @Override
    public int getViewTypeCount() {

        return getCount();
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }
}


