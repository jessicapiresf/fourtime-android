package android.tests.jessicafreitas.testeandroidfourtime.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.tests.jessicafreitas.testeandroidfourtime.Adapter.SummarizedAdapter;
import android.tests.jessicafreitas.testeandroidfourtime.Domain.Summarized;
import android.tests.jessicafreitas.testeandroidfourtime.MainActivity;
import android.tests.jessicafreitas.testeandroidfourtime.R;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.androidannotations.annotations.EFragment;

/**
 * Created by Jessica on 08/08/16.
 */

@EFragment(R.layout.fragment_summarized)
public class SummarizedFragment extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        ((MainActivity) getActivity()).setTitle(getResources().getString(R.string.menu_extrato_resumido));
        View view =  inflater.inflate(R.layout.fragment_summarized, container, false);

        Summarized[] summarized = {
                new Summarized("Janeiro","R$ 18.273,00"),
                new Summarized("Fevereiro","R$ 343,20"),
                new Summarized("Março","R$ 13,00"),
                new Summarized("Abril","R$ 233,00"),
                new Summarized("Maio","R$ 12.873,00"),
                new Summarized("Junho","R$ 13,00"),
                new Summarized("Agosto","R$ 133,00")
        };

        SummarizedAdapter adapter = new SummarizedAdapter(getContext(), R.layout.row_summarized, summarized, false);

        ListView lv = (ListView) view.findViewById(R.id.list);
        lv.setAdapter(adapter);

        return view;

    }

}