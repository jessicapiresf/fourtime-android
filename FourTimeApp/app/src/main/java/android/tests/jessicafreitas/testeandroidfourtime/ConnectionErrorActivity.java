package android.tests.jessicafreitas.testeandroidfourtime;

import android.app.Activity;
import android.content.Intent;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;

/**
 * Created by Jessica on 06/08/16.
 */

@EActivity(R.layout.activity_connection_error)
public class ConnectionErrorActivity extends Activity{


    @Click(R.id.tentarNovamente)
    void tentarNovamente(){
        Intent mainIntent = new Intent(ConnectionErrorActivity.this, MainActivity_.class);
        ConnectionErrorActivity.this.startActivity(mainIntent);
        ConnectionErrorActivity.this.finish();
    }
}
