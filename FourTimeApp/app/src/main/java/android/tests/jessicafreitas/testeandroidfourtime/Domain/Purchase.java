package android.tests.jessicafreitas.testeandroidfourtime.Domain;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Jessica on 08/08/16.
 */
public class Purchase {

    @SerializedName(value="list")
    public List<purchaseClass> purchaseList;

    public void setPurchase(List<purchaseClass> purchase) {
        this.purchaseList = purchase;

    }

    public static class purchaseClass {
        int index;
        int typecode;
        String establishment;
        float value;
        String data;
        String type;

        public int getIndex() {
            return index;
        }

        public void setIndex(int index) {
            this.index = index;
        }

        public int getTypecode() {
            return typecode;
        }

        public void setTypecode(int typecode) {
            this.typecode = typecode;
        }

        public String getEstablishment() {
            return establishment;
        }

        public void setEstablishment(String establishment) {
            this.establishment = establishment;
        }

        public float getValue() {
            return value;
        }

        public void setValue(float value) {
            this.value = value;
        }

        public String getData() {
            return data;
        }

        public void setData(String data) {
            this.data = data;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

}
