package android.tests.jessicafreitas.testeandroidfourtime.Adapter;

import android.content.Context;
import android.tests.jessicafreitas.testeandroidfourtime.Domain.Purchase;
import android.tests.jessicafreitas.testeandroidfourtime.R;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Jessica on 09/08/16.
 */
public class DetailedAdapter extends ArrayAdapter<Purchase.purchaseClass> {

    Context mContext;
    int layoutResourceId;
    Purchase.purchaseClass data[] = null;

    public DetailedAdapter(Context context, int resource, Purchase.purchaseClass[] data, boolean profile) {
        super(context, resource, data);

        this.layoutResourceId = resource;
        this.mContext = context;
        this.data = data;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView==null){
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(layoutResourceId, parent, false);
        }

        if (position == 0 && position==getCount()-1){
            View viewLineUp = (View) convertView.findViewById(R.id.viewLineUp);
            View viewLineBottom = (View) convertView.findViewById(R.id.viewLineBottom);
            viewLineBottom.setVisibility(View.INVISIBLE);
            viewLineUp.setVisibility(View.INVISIBLE);
        }
        else if(position == 0){
            View viewLineUp = (View) convertView.findViewById(R.id.viewLineUp);
            viewLineUp.setVisibility(View.INVISIBLE);
        }
        else if(position==getCount()-1){
            View viewLineBottom = (View) convertView.findViewById(R.id.viewLineBottom);
            viewLineBottom.setVisibility(View.INVISIBLE);
        }

        Purchase.purchaseClass sales = data[position];

        TextView nomeEstabelecimento = (TextView) convertView.findViewById(R.id.nomeEstabelecimento);
        TextView tipoEstabelecimento = (TextView) convertView.findViewById(R.id.tipoEstabelecimento);
        TextView valor = (TextView) convertView.findViewById(R.id.valor);
        TextView data = (TextView) convertView.findViewById(R.id.data);
        ImageView icone = (ImageView) convertView.findViewById(R.id.icon);

        nomeEstabelecimento.setText(sales.getEstablishment());
        valor.setText(String.format("R$ %.2f", sales.getValue()));
        tipoEstabelecimento.setText(sales.getType());
        data.setText(sales.getData());

        if(sales.getTypecode() == 3){
            icone.setBackgroundResource(R.mipmap.ic_car);
        }
        else if(sales.getTypecode() == 6){
            icone.setBackgroundResource(R.mipmap.ic_gift);
        }
        else if(sales.getTypecode() == 2 || sales.getTypecode() == 5){
            icone.setBackgroundResource(R.mipmap.ic_food);
        }
        else{
            icone.setBackgroundResource(R.mipmap.ic_gift);
        }

        return convertView;

    }

    @Override
    public int getViewTypeCount() {

        return getCount();
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }
}


