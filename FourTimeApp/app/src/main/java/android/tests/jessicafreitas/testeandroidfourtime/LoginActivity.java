package android.tests.jessicafreitas.testeandroidfourtime;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.tests.jessicafreitas.testeandroidfourtime.Helper.InputValidate;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

/**
 * Created by Jessica on 06/08/16.
 */

@EActivity(R.layout.activity_login)
public class LoginActivity extends Activity {


    @ViewById(R.id.loginEmail)
    EditText loginEmail;

    @ViewById(R.id.layoutloginEmail)
    TextInputLayout layoutloginEmail;

    @ViewById(R.id.loginSenha)
    EditText loginSenha;

    @ViewById(R.id.layoutloginSenha)
    TextInputLayout layoutloginSenha;


    @AfterViews
    void setupViews() {

        loginEmail.addTextChangedListener(watch);
        loginSenha.addTextChangedListener(watch);
    }

    TextWatcher watch = new TextWatcher() {

        @Override
        public void afterTextChanged(Editable arg0) {
            // TODO Auto-generated method stub

        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
            // TODO Auto-generated method stub

        }


        @Override
        public void onTextChanged(CharSequence s, int a, int b, int c) {
            // TODO Auto-generated method stub

            if(InputValidate.isEmailValid(loginEmail.getText().toString())){
                layoutloginEmail.setHint("E-mail");
                layoutloginEmail.setHintTextAppearance(R.style.TextAppearence_TextInputLayout_Grey);
            }
            else{
                layoutloginEmail.setHintTextAppearance(R.style.TextAppearence_TextInputLayout_Pinkish);
                layoutloginEmail.setHint("E-mail inválido");
            }

            if(InputValidate.isPasswordValid(loginSenha.getText().toString())){
                layoutloginSenha.setHint("Senha");
                layoutloginSenha.setHintTextAppearance(R.style.TextAppearence_TextInputLayout_Grey);
            }
            else{
                layoutloginSenha.setHintTextAppearance(R.style.TextAppearence_TextInputLayout_Pinkish);
                layoutloginSenha.setHint("Sua senha deve conter no mínimo 8 dígitos");
            }

        }
    };

    @Click(R.id.buttonLogin)
    void login() {

        if (fieldsValidade()) {

            /*Deve fazer a autenticacao antes de fazer o login*/

            Intent mainIntent = new Intent(LoginActivity.this, MainActivity_.class);
            LoginActivity.this.startActivity(mainIntent);
            LoginActivity.this.finish();

        }

    }


    public boolean fieldsValidade() {

        boolean isValid = true;

        if (!InputValidate.isEmailValid(loginEmail.getText().toString())) {
            layoutloginEmail.setHintTextAppearance(R.style.TextAppearence_TextInputLayout_Pinkish);
            layoutloginEmail.setHint("E-mail inválido");
            isValid = false;
        }

        if (!InputValidate.isPasswordValid(loginSenha.getText().toString())) {
            layoutloginSenha.setHintTextAppearance(R.style.TextAppearence_TextInputLayout_Pinkish);
            layoutloginSenha.setHint("Sua senha deve conter no mínimo 8 dígitos");
            isValid = false;
        }
        return isValid;
    }


    @Click(R.id.registerUser)
    void register() {
        Intent mainIntent = new Intent(LoginActivity.this, RegisterActivity_.class);
        LoginActivity.this.startActivity(mainIntent);

    }

    @Click(R.id.forgotPass)
    void forgotPass() {
        Intent mainIntent = new Intent(LoginActivity.this, ForgotPassActivity_.class);
        LoginActivity.this.startActivity(mainIntent);

    }

}
