package android.tests.jessicafreitas.testeandroidfourtime.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.tests.jessicafreitas.testeandroidfourtime.MainActivity;
import android.tests.jessicafreitas.testeandroidfourtime.R;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.androidannotations.annotations.EFragment;

/**
 * Created by Jessica on 08/08/16.
 */

@EFragment(R.layout.fragment_profile)
public class ProfileFragment extends Fragment{


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        ((MainActivity) getActivity()).setTitle(getResources().getString(R.string.menu_editar_perfil));
        View view =  inflater.inflate(R.layout.fragment_profile, container, false);


        return view;

    }

}