package android.tests.jessicafreitas.testeandroidfourtime.Api;

/**
 * Created by Jessica on 09/08/16.
 */

import android.tests.jessicafreitas.testeandroidfourtime.Domain.Month;
import android.tests.jessicafreitas.testeandroidfourtime.Domain.Purchase;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface MonthApi{

    String ENDPOINT ="http://api.mockup.fourtime.com";

    @GET("/statement/months")
    Call<Month> getMonths();

    @GET("/statement/{month}")
    Call<Purchase> getPurchase(@Path("month") int month);

}
